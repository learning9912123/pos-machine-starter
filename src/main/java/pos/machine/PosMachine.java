package pos.machine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        Map<String, Integer> barcodeMap = groupBarcodes(barcodes);

        List<ReceiptItem> receiptItems = buildReceiptItemList(barcodeMap);

        Receipt receipt = buildReceipt(receiptItems);

        return receipt.toString();
    }

    private List<ReceiptItem> buildReceiptItemList(Map<String, Integer> barcodeMap) {
        List<ReceiptItem> receiptItems = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : barcodeMap.entrySet()) {
            Item item = getItemByBarcode(entry.getKey());
            int total = item.getPrice() * entry.getValue();
            ReceiptItem receiptItem = new ReceiptItem(item.getName(), entry.getValue(), item.getPrice(), total);
            receiptItems.add(receiptItem);
        }
        return receiptItems;
    }

    public Receipt buildReceipt(List<ReceiptItem> receiptItems) {
        int totalPrice = 0;
        for (ReceiptItem receiptItem : receiptItems) {
            totalPrice += receiptItem.getSubTotal();
        }
        Receipt receipt = new Receipt(receiptItems, totalPrice);
        return receipt;
    }

    private Item getItemByBarcode(String barcode) {
        for (Item currentItem : ItemsLoader.loadAllItems()) {
            if (currentItem.getBarcode().equals(barcode)) {
                return currentItem;
            }
        }
        return null;
    }

    private Map<String, Integer> groupBarcodes(List<String> barcodes) {
        Map<String, Integer> map = new HashMap<>();
        barcodes.forEach(barcode -> {
            if (map.containsKey(barcode)) {
                int count = map.get(barcode) + 1;
                map.put(barcode, count);
            } else {
                map.put(barcode, 1);
            }
        });
        return map;
    }
}
